<?php

/**
 * @file
 * Install file for simple fb connect module. Checks requirements.
 */

/**
 * Implements hook_requirements().
 */
function fb_php_requirements($phase) {
  $requirements = array();
  $t = get_t();

  if ($phase == 'install') {

    if (!function_exists('curl_init')) {
      $requirements['curl']['severity'] = REQUIREMENT_ERROR;
      $requirements['curl']['description'] = $t('Simple FB Connect could not be installed. The cURL library is not installed. Please check the <a href="@url">PHP cURL documentation</a> for information on how to correct this.', array('@url' => 'http://www.php.net/manual/en/curl.setup.php'));
    }

    if (!function_exists('json_decode')) {
      $requirements['json']['severity'] = REQUIREMENT_ERROR;
      $requirements['json']['description'] = $t('Simple FB Connect could not be installed. The JSON library is not installed. Facebook needs the JSON PHP extension.');
    }

    if (version_compare(phpversion(), '5.4.0', '<')) {
      $requirements['php_version']['title'] = $t('PHP version');
      $requirements['php_version']['value'] = check_plain(phpversion());
      $requirements['php_version']['severity'] = REQUIREMENT_ERROR;
      $requirements['php_version']['description'] = $t('Simple FB Connect could not be installed. Facebook PHP SDK v4 requires PHP 5.4 or higher.');
    }

    // Make sure that Libraries module is loaded
    if (!function_exists('libraries_get_path')) {
      module_load_include('module', 'libraries');
    }
    // Try to check that we have version 4.0.x of the SDK library.
    // - libraries_detect does not work in the install phase so we can't easily check the exact version
    // - We check if sites/all/libraries/facebook-php-sdk-v4/README.md is found
    $path = libraries_get_path('facebook-php-sdk-v4');
    if (!is_file($path . '/README.md')) {
      $requirements['facebook-php-sdk-v4']['severity'] = REQUIREMENT_ERROR;
      $requirements['facebook-php-sdk-v4']['description'] = $t('Simple FB Connect could not be installed. Facebook PHP SDK library version 4.0.x not found. See README.txt.');
    }
  }

  if ($phase == 'runtime') {
    $sdk = libraries_detect('facebook-php-sdk-v4');
    if (!is_array($sdk) || !$sdk['installed']) {
      $requirements['facebook-php-sdk-v4']['title'] = $t('Facebook PHP SDK');
      $requirements['facebook-php-sdk-v4']['value'] = $t('Not installed');
      $requirements['facebook-php-sdk-v4']['severity'] = REQUIREMENT_ERROR;
      $requirements['facebook-php-sdk-v4']['description'] = $t('Facebook PHP SDK library not found. See README.txt');
    }
    elseif (($sdk['version'] < '5.0') || ($sdk['version'] >= '6.0')) {
      $requirements['facebook-php-sdk-v4']['title'] = $t('Facebook PHP SDK');
      $requirements['facebook-php-sdk-v4']['value'] = $t('Unsupported version detected');
      $requirements['facebook-php-sdk-v4']['severity'] = REQUIREMENT_ERROR;
      $requirements['facebook-php-sdk-v4']['description'] = $t('Version @version detected. See README.txt', array('@version' => $sdk['version']));
    }
    else {
      $requirements['facebook-php-sdk-v4']['title'] = $t('Facebook PHP SDK');
      $requirements['facebook-php-sdk-v4']['value'] = $t('Version @version detected.', array('@version' => $sdk['version']));
      $requirements['facebook-php-sdk-v4']['severity'] = REQUIREMENT_OK;
    }
  }

  return $requirements;
}


/**
 * Implements hook_install().
 */
function fb_php_install() {

}

/**
 * Implements hook_uninstall().
 */
function fb_php_uninstall() {
  variable_del('fb_sdk_appid');
  variable_del('fb_sdk_secret');
  variable_del('fb_sdk_cookies');
  variable_del('fb_sdk_api');
}

/**
 * Implements hook_enable().
 */
function fb_php_enable() {
  drupal_set_message(t('Facebook PHP SDK enabled. <a href="@path">Check Facebook SDK settings</a>.', array('@path' => url('admin/config/services/facebook.sdk'))));
}
