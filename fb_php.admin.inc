<?php

/**
 * @file
 * Administration page callbacks for the simple fb connect module.
 */
function fb_php_sdk_settings($form, &$form_state) {
  // Check that Facebook PHP SDK is properly installed and that the version is 4.0.x
  $sdk = libraries_detect('facebook-php-sdk-v4');
  if (!is_array($sdk) || !$sdk['installed'] || $sdk['version'] < '5.0' || $sdk['version'] >= '6.0') {
    drupal_set_message(t('Facebook PHP SDK v4 not properly installed. Check README.txt and <a href="@url">Drupal status report</a> for further details!', array('@url' => url('admin/reports/status'))), 'error');
  }

  $form['fb_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook App settings'),
    '#description' => t('<p>You need to first create a Facebook App at <a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a>.</p><p>Installation and configuration instructions can be found from the README.txt. Also check <a href="@url">Drupal account settings</a></p>', array('@url' => url('admin/config/people/accounts'))),
  );

  $form['fb_settings']['fb_sdk_appid'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('App ID'),
    '#default_value' => variable_get('fb_sdk_appid', NULL),
    '#description' => t('Copy the App ID of your Facebook App here'),
  );

  $form['fb_settings']['fb_sdk_secret'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('App Secret'),
    '#default_value' => variable_get('fb_sdk_secret', NULL),
    '#description' => t('Copy the App Secret of your Facebook App here'),
  );

  $form['fb_sdk_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('SDK settings'),
  );

  $form['fb_sdk_settings']['fb_sdk_cookies'] = array(
    '#required' => TRUE,
    '#type' => 'select',
    '#title' => t('Cookies'),
    '#description' => t('Enable cookies to pass data between the Javascript & PHP SDKs.'),
    '#options' => array(
      'true' => t('Enabled'),
      'false' => t('Disabled'),
    ),
    '#default_value' => variable_get('fb_sdk_cookies', 'true'),
  );

  $form['graph_api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Graph API settings'),
  );

  $form['graph_api_settings']['fb_sdk_api'] = array(
    '#required' => TRUE,
    '#title' => t('Graph API Version'),
    '#type' => 'select',
    '#description' => t('Select which version of the Javascript SDK to use.'),
    '#options' => array(
      '2.5' => t('Version 2.5'),
    ),
    '#default_value' => variable_get('fb_sdk_api', '2.5'),
  );
  return system_settings_form($form);
}
